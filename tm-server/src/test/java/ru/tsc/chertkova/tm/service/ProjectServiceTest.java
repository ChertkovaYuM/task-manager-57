package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.tsc.chertkova.tm.api.repository.model.IProjectRepository;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.exception.field.UserIdEmptyException;
import ru.tsc.chertkova.tm.marker.DataBaseCategory;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.model.ProjectRepository;
import ru.tsc.chertkova.tm.service.model.ProjectService;
import ru.tsc.chertkova.tm.service.model.TaskService;
import ru.tsc.chertkova.tm.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

import static ru.tsc.chertkova.tm.constant.ProjectTestData.*;
import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(DataBaseCategory.class)
public class ProjectServiceTest {

    @Nullable
    private static IPropertyService propertyService;

    @Nullable
    private static IConnectionService connectionService;

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserService userService = new UserService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final ITaskService taskService = new TaskService();

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void initConnectionService() {
        propertyService = new PropertyService();
    }

    @AfterClass
    public static void destroy() {
        connectionService.close();
    }

    @Before
    public void init() {
        for (Project p : PROJECT_LIST) {
            projectRepository.add(p);
        }
    }

    @After
    public void end() {
        taskService.clear(USER1.getId());
        for (@NotNull final Project project : projectService.findAll(USER1.getId())) {
            if (USER1.getId().equals(project.getUser().getId()))
                projectService.remove(USER1.getId(), project);
        }
        for (@NotNull final Task task : taskService.findAll(USER2.getId())) {
            if (USER1.getId().equals(task.getUser().getId()))
                taskService.removeById(USER1.getId(), task.getId());
        }
        userService.removeByLogin(USER1.getLogin());
        userService.removeByLogin(USER2.getLogin());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(PROJECT_LIST, projectService.findAll(ADMIN1.getId()));
    }

    @Test
    public void FindAllByUserId() {
        Assert.assertEquals(ADMIN1_PROJECT_LIST, projectService.findAll(ADMIN1.getId()));
        thrown.expect(UserIdEmptyException.class);
        projectService.findAll(ADMIN1.getId());
    }

    @Test
    public void clear() {
        projectService.clear(ADMIN1.getId());
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void clearByUserId() {
        projectService.clear(USER1.getId());
        Assert.assertTrue(projectRepository.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        projectService.clear(null);
    }

    @Test
    public void remove() {
        @NotNull final List<Project> list = new ArrayList<>(projectRepository.findAll());
        @Nullable final Project removed = projectService.remove(USER1.getId(), USER1_PROJECT2);
        Assert.assertEquals(USER1_PROJECT2, removed);
        list.remove(USER1_PROJECT2);
        Assert.assertEquals(list, projectRepository.findAll());
        Assert.assertNull(projectService.remove(null, null));
        Assert.assertNull(projectService.remove(null, USER1_PROJECT2));
    }

    @Test
    public void removeByUserId() {
        @NotNull final List<Project> list = new ArrayList<>(projectRepository.findAll());
        projectService.remove(USER1.getId(), USER1_PROJECT2);
        list.remove(USER1_PROJECT2);
        Assert.assertEquals(list, projectRepository.findAll());
        projectService.remove(USER1.getId(), null);
        Assert.assertEquals(list, projectRepository.findAll());
        projectService.remove(USER1.getId(), USER2_PROJECT1);
        Assert.assertEquals(list, projectRepository.findAll());
        thrown.expect(UserIdEmptyException.class);
        projectService.remove(null, USER2_PROJECT1);
        Assert.assertEquals(list, projectRepository.findAll());
    }

    @Test
    public void createByNameAndUserId() {
        projectRepository.clear();
        @NotNull final Project expected = new Project();
        expected.setName(USER1_PROJECT1.getName());
        expected.setUser(USER1);
        projectService.add(USER1_PROJECT1);
        @NotNull final Project created = projectRepository.findAll().get(0);
        thrown.expect(UserIdEmptyException.class);
        projectService.add(USER1_PROJECT1);
        thrown.expect(NameEmptyException.class);
        projectService.add(null);
    }

}
