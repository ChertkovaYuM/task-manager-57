package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.tsc.chertkova.tm.api.repository.model.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IConnectionService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.marker.DataBaseCategory;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.repository.model.UserRepository;
import ru.tsc.chertkova.tm.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

import static ru.tsc.chertkova.tm.constant.UserTestData.*;

@Category(DataBaseCategory.class)
public class UserServiceTest {

    @Nullable
    private static IPropertyService propertyService;

    @Nullable
    private static IConnectionService connectionService;

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService service = new UserService(propertyService);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void initConnectionService() {
        propertyService = new PropertyService();
    }

    @AfterClass
    public static void destroy() {
        connectionService.close();
    }

    @Before
    public void init() {
        for (User u :
                USER_LIST) {
            userRepository.add(u);
        }
    }

    @After
    public void end() {
        userRepository.clear();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER_LIST, service.findAll(ADMIN1.getId()));
    }

    @Test
    public void clear() {
        service.clear(ADMIN1.getId());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void remove() {
        @NotNull final List<User> list = new ArrayList<>(userRepository.findAll());
        @Nullable final User removed = service.remove(USER1);
        Assert.assertEquals(USER1, removed);
        list.remove(USER1);
        Assert.assertEquals(list, userRepository.findAll());
        Assert.assertNull(service.remove(null));
        Assert.assertNull(service.remove(USER1));
    }

    @Test
    public void findById() {
        Assert.assertEquals(
                service.findById(USER1.getId()),
                USER1
        );
        thrown.expect(IdEmptyException.class);
        service.findById(null);
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(
                service.findByLogin(USER1.getLogin()),
                USER1
        );
        thrown.expect(LoginEmptyException.class);
        service.findByLogin(null);
    }

    @Test
    public void removeById() {
        Assert.assertTrue(service.findAll(ADMIN1.getId()).contains(USER1));
        service.removeById(USER1.getId());
        Assert.assertFalse(service.findAll(ADMIN1.getId()).contains(USER1));
    }

    @Test
    public void create() {
        userRepository.clear();
        service.add(new User(USER1.getLogin(), "password", "dhsf", "f1", "f2", "f3"));
        @Nullable final User created = userRepository.findByLogin(USER1.getLogin());
        Assert.assertNotNull(userRepository.findByLogin(USER1.getLogin()));
        Assert.assertEquals(USER1.getEmail(), created.getEmail());
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
    }

    @Test
    public void createWithRole() {
        userRepository.clear();
        User user = new User(USER1.getLogin(), "password", "dhsf", "f1", "f2", "f3");
        user.setRole(USER1.getRole());
        service.add(user);
        @Nullable final User created = userRepository.findByLogin(USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), created.getLogin());
        Assert.assertEquals(USER1.getRole(), created.getRole());
    }

    @Test
    public void updateById() {
        userRepository.clear();
        @NotNull final User actual = new User();
        actual.setId(ADMIN1.getId());
        userRepository.add(actual);
        service.updateUser(
                actual.getId(),
                ADMIN1.getFirstName(),
                ADMIN1.getLastName(),
                ADMIN1.getMiddleName()
        );
    }

    @Test
    public void updatePasswordById() {
        userRepository.clear();
        @NotNull final User user1 = new User();
        user1.setId(USER1.getId());
        userRepository.add(user1);
        @NotNull final User user2 = new User();
        user2.setId(USER2.getId());
        userRepository.add(user2);
        service.setPassword(USER1.getId(), "password");
        service.setPassword(USER2.getId(), "password");
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
    }

    @Test
    public void lockUserByLogin() {
        userRepository.clear();
        @NotNull final User user1 = new User();
        user1.setLogin(USER1.getLogin());
        user1.setLocked(false);
        userRepository.add(user1);
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(user1.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        userRepository.clear();
        @NotNull final User user1 = new User();
        user1.setLogin(USER1.getLogin());
        user1.setLocked(true);
        userRepository.add(user1);
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(user1.getLocked());
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(userRepository.findAll().contains(USER1));
        service.removeByLogin(USER1.getLogin());
        Assert.assertFalse(userRepository.findAll().contains(USER1));
    }

}
