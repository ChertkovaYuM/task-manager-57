package ru.tsc.chertkova.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class ProjectRepositoryDTO extends AbstractUserOwnerModelRepositoryDTO<ProjectDTO>
        implements IProjectRepositoryDTO {

    @Override
    public void add(@NotNull final ProjectDTO model) {
        super.add(model);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("FROM ProjectDTO", ProjectDTO.class).getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(ProjectDTO.class, id));
    }

    @Override
    public void update(@NotNull final ProjectDTO model) {
        entityManager.createQuery("UPDATE ProjectDTO e SET e.name=:name AND e.description=:description WHERE e.user_id=:userId AND e.id=:id")
                .setParameter("name", model.getName())
                .setParameter("description", model.getDescription())
                .setParameter("id", model.getId())
                .setParameter("userId", model.getUserId())
                .executeUpdate();
    }

    @Override
    public void add(@NotNull final String userId,
                    @NotNull final ProjectDTO model) {
        model.setUserId(userId);
        add(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM ProjectDTO e WHERE e.user_id=:userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("FROM ProjectDTO e WHERE e.user_id=:userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull final String userId,
                               @NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e", Integer.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.user_id=:userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId,
                           @NotNull final String id) {
        entityManager.remove(entityManager.getReference(ProjectDTO.class, id));
    }

    @Override
    public void update(@NotNull final String userId,
                       @NotNull final ProjectDTO model) {
        model.setUserId(userId);
        update(model);
    }

    @Override
    public void changeStatus(@NotNull final String id,
                             @NotNull final String userId,
                             @NotNull final String status) {
        entityManager.createQuery("UPDATE ProjectDTO e SET e.status=:status WHERE e.user_id=:userId AND e.id=:id")
                .setParameter("status", status)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int existsById(@Nullable final String id) {
        return entityManager
                .createQuery("FROM ProjectDTO e WHERE e.id=:id", Integer.class)
                .setParameter("id", id)
                .setMaxResults(1).getSingleResult();

    }

}
