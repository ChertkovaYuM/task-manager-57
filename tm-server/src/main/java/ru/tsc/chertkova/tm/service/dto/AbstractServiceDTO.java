package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.IService;
import ru.tsc.chertkova.tm.model.AbstractModel;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractServiceDTO<M extends AbstractModel> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
