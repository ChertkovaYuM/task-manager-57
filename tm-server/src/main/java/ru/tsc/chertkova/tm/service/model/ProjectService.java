package ru.tsc.chertkova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.repository.model.IProjectRepository;
import ru.tsc.chertkova.tm.api.repository.model.ITaskRepository;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractUserOwnerService<Project> implements IProjectService {

    @NotNull
    public IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(project.getUser()).orElseThrow(UserNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Optional.ofNullable(project.getName()).orElseThrow(NameEmptyException::new);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String id,
                              @Nullable final String userId,
                              @Nullable final String name,
                              @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @Nullable Project project;
        try {
            entityManager.getTransaction().begin();
            project = Optional.ofNullable(findById(userId, id)).orElseThrow(ProjectNotFoundException::new);
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            project = findById(userId, id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(@Nullable final String userId,
                                           @Nullable final String id,
                                           @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @Nullable Project project;
        try {
            entityManager.getTransaction().begin();
            projectRepository.changeStatus(id, userId, status.getDisplayName());
            entityManager.getTransaction().commit();
            project = findById(userId, id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull int count = projectRepository.existsById(id);
        return count > 0;
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId,
                            @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @Nullable Project project;
        try {
            entityManager.getTransaction().begin();
            project = projectRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @NotNull final Project project;
        try {
            entityManager.getTransaction().begin();
            project = Optional.ofNullable(findById(userId, id)).orElseThrow(ProjectNotFoundException::new);
            projectRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public Project remove(@Nullable final String userId,
                          @Nullable final Project project) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(findById(project.getUser().getId(), project.getId())).orElseThrow(ProjectNotFoundException::new);
        removeById(project.getUser().getId(), project.getId());
        return project;
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        int size = 0;
        try {
            entityManager.getTransaction().begin();
            size = projectRepository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return size;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @Nullable List<Project> projects;
        try {
            entityManager.getTransaction().begin();
            projects = projectRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    public List<Project> addAll(@NotNull final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (Project p : projects) {
                projectRepository.add(p);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    public List<Project> removeAll(@Nullable final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (Project p : projects) {
                projectRepository.removeById(p.getUser().getId(), p.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @Override
    @SneakyThrows
    public Task bindTaskToProject(@Nullable final String userId,
                                  @Nullable final String projectId,
                                  @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        @Nullable Task task;
        try {
            @NotNull final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
            entityManager.getTransaction().begin();
            if (projectRepository.existsById(projectId) < 1) throw new ProjectNotFoundException();
            Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(TaskNotFoundException::new);
            taskRepository.bindTaskToProject(taskId, projectId, userId);
            entityManager.getTransaction().commit();
            task = taskRepository.findById(userId, taskId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

}
