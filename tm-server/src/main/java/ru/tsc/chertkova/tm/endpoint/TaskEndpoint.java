package ru.tsc.chertkova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.chertkova.tm.api.service.IProjectService;
import ru.tsc.chertkova.tm.api.service.ITaskService;
import ru.tsc.chertkova.tm.dto.request.task.*;
import ru.tsc.chertkova.tm.dto.response.task.*;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Session;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public @NotNull TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable Task task = getProjectService().bindTaskToProject(user.getId(), projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskUnbindToProjectResponse unbindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindToProjectRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable Task task = getProjectService().bindTaskToProject(user.getId(), projectId, taskId);
        return new TaskUnbindToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable Task task = getTaskService().changeTaskStatusById(user.getId(), id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        getTaskService().clear(user.getId());
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService()
                .changeTaskStatusById(user.getId(), id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);

    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Task task = new Task(name, description, projectId);
        task.setUser(user);
        getTaskService().add(task);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @Nullable final Session session = check(request);
        System.out.println(session.toString());
        @Nullable final User user = session.getUser();
        @Nullable final List<Task> tasks = getTaskService().findAll(user.getId());
        return new TaskListResponse(tasks);

    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService().removeById(user.getId(), id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService().findById(user.getId(), id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String id = request.getId();
        @Nullable Task task = getTaskService()
                .changeTaskStatusById(user.getId(), id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Task task = getTaskService().updateById(user.getId(), id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowListByProjectIdResponse showListByProjectIdTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowListByProjectIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = session.getUser();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> tasks = getTaskService().findAllByProjectId(user.getId(), projectId);
        return new TaskShowListByProjectIdResponse(tasks);
    }

}
