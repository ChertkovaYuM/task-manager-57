package ru.tsc.chertkova.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class TaskRepositoryDTO
        extends AbstractUserOwnerModelRepositoryDTO<TaskDTO>
        implements ITaskRepositoryDTO {

    @Override
    public void add(@NotNull TaskDTO model) {
        super.add(model);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId) {
        return entityManager.createQuery("FROM TaskDTO e WHERE e.user_id=:userId AND e.project_id=:projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void changeStatus(
            @NotNull final String id,
            @NotNull final String userId,
            @NotNull final String status) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.status=:status WHERE e.user_id=:userId AND e.id=:id")
                .setParameter("status", status)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int existsById(@Nullable final String id) {
        return entityManager
                .createQuery("FROM TaskDTO e WHERE e.id=:id", Integer.class)
                .setParameter("id", id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void bindTaskToProject(@NotNull final String taskId,
                                  @NotNull final String projectId,
                                  @NotNull final String userId) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.project_id=:projectId WHERE e.user_id=:userId AND e.id=:taskId")
                .setParameter("taskId", taskId)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("FROM TaskDTO", TaskDTO.class).getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e", Integer.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(TaskDTO.class, id));
    }

    @Override
    public void update(@NotNull final TaskDTO model) {
        entityManager.createQuery("UPDATE TaskDTO e SET e.name=:name AND e.description=:description WHERE e.user_id=:userId AND e.id=:id")
                .setParameter("name", model.getName())
                .setParameter("description", model.getDescription())
                .setParameter("id", model.getId())
                .setParameter("userId", model.getUserId())
                .executeUpdate();
    }

    @Override
    public void add(@NotNull final String userId,
                    @NotNull TaskDTO model) {
        model.setUserId(userId);
        add(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM TaskDTO e WHERE e.user_id=:userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("FROM TaskDTO e WHERE e.user_id=:userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public TaskDTO findById(@NotNull final String userId,
                            @NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e WHERE e.user_id=:userId", Integer.class)
                .setParameter("userId", userId)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId,
                           @NotNull final String id) {
        entityManager.remove(entityManager.getReference(TaskDTO.class, id));
    }

    @Override
    public void update(@NotNull final String userId,
                       @NotNull final TaskDTO model) {
        model.setUserId(userId);
        update(model);
    }

}
