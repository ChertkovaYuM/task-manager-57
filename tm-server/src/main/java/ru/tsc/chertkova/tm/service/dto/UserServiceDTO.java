package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.repository.model.IUserRepository;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.EmailEmptyException;
import ru.tsc.chertkova.tm.exception.field.EmailExistsException;
import ru.tsc.chertkova.tm.exception.field.IdEmptyException;
import ru.tsc.chertkova.tm.exception.field.NameEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginExistsException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserServiceDTO extends AbstractServiceDTO<User> implements IUserService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    public IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        int count = 0;
        try {
            entityManager.getTransaction().begin();
            count = userRepository.existsById(id);
        } finally {
            entityManager.close();
        }
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User remove(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        Optional.ofNullable(userRepository.findById(user.getId())).orElseThrow(UserNotFoundException::new);
        removeById(user.getId());
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        removeById(user.getId());
        return user;
    }

    @Override
    @Nullable
    public User removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findById(id);
            userRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Override
    public void clear(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExists(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        long count = 0;
        try {
            entityManager.getTransaction().begin();
            count = userRepository.isLoginExist(login);
        } finally {
            entityManager.close();
        }
        return count > 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmailExists(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        long count = 0;
        try {
            entityManager.getTransaction().begin();
            count = userRepository.isEmailExist(email);
        } finally {
            entityManager.close();
        }
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId,
                            @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            Optional.ofNullable(userRepository.findById(userId)).orElseThrow(UserNotFoundException::new);
            userRepository.setPassword(userId, HashUtil.salt(propertyService, password));
            entityManager.getTransaction().commit();
            user = userRepository.findById(userId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUser(@Nullable final String userId,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            user = Optional.ofNullable(userRepository.findById(userId))
                    .orElseThrow(UserNotFoundException::new);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            user = userRepository.findById(userId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            userRepository.setLockedFlag(login, true);
            entityManager.getTransaction().commit();
            user = userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            userRepository.setLockedFlag(login, false);
            entityManager.getTransaction().commit();
            user = userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User add(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
        Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            if (userRepository.isLoginExist(user.getLogin()) > 0) throw new LoginExistsException();
            if (userRepository.isEmailExist(user.getEmail()) > 0) throw new EmailExistsException();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User updateById(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String middleName,
                           @Nullable final String lastName) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            user = Optional.ofNullable(userRepository.findById(id))
                    .orElseThrow(UserNotFoundException::new);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            user = userRepository.findById(id);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return user;
    }

    @Override
    @Nullable
    public User findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable User user;
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findById(id);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public int getSize(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable int count = 0;
        try {
            entityManager.getTransaction().begin();
            count = userRepository.getSize();
        } finally {
            entityManager.close();
        }
        return count;
    }

    @Override
    @Nullable
    public List<User> findAll(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable List<User> users;
        try {
            entityManager.getTransaction().begin();
            users = userRepository.findAll();
        } finally {
            entityManager.close();
        }
        return users;
    }

    @Override
    @Nullable
    public List<User> addAll(@Nullable List<User> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (User user : users) {
                Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
                Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
                Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
                userRepository.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return users;
    }

    @Override
    @Nullable
    public List<User> removeAll(@Nullable final List<User> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        @NotNull final IUserRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (User u : users) {
                userRepository.removeById(u.getId());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        return users;
    }

}
