package ru.tsc.chertkova.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.api.repository.model.IAbstractRepository;
import ru.tsc.chertkova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel>
        implements IAbstractRepository<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull M model) {
        entityManager.merge(model);
    }

    @Override
    public abstract void clear();

    @Override
    @NotNull
    public abstract List<M> findAll();

    @Override
    @Nullable
    public abstract M findById(@NotNull String id);

    @Override
    public abstract int getSize();

    @Override
    public abstract M removeById(@NotNull String id);

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
