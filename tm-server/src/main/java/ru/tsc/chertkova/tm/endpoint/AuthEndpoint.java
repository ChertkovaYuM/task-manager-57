package ru.tsc.chertkova.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.dto.request.user.UserLoginRequest;
import ru.tsc.chertkova.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.chertkova.tm.dto.response.user.UserLoginResponse;
import ru.tsc.chertkova.tm.dto.response.user.UserLogoutResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    final static String REQUEST = "Request";

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        return new UserLogoutResponse();
    }

}
