package ru.tsc.chertkova.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.model.IWBS;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.listener.EntityListener;
import ru.tsc.chertkova.tm.util.DateUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AssociationOverride(name = "user", joinColumns = @JoinColumn(name = "user_id"))
public final class Project extends AbstractFieldsModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name,
                   @NotNull final Status status,
                   @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return getId() + " - " + name + " : " + description + ", " + status + ", " + DateUtil.toString(dateBegin);
    }

}
