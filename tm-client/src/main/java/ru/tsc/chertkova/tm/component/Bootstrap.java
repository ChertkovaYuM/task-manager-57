package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractListener;
import ru.tsc.chertkova.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.chertkova.tm.exception.system.CommandNotSupportedException;
import ru.tsc.chertkova.tm.util.SystemUtil;
import ru.tsc.chertkova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private IListenerService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @Nullable
    @Autowired
    private ApplicationEventPublisher publisher;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        initCommands(listeners);
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void run(@Nullable final String[] args) {
        if (args.length > 0) processArgument(args[0]);
        prepareStartup();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[FAIL]");
        }
    }

    @SneakyThrows
    public void initCommands(@Nullable final AbstractListener[] listeners) {
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener == null) return;
            commandService.add(listener);
        }
    }

    @SneakyThrows
    private void processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty())
            throw new ArgumentNotSupportedException(argument);
        @Nullable final AbstractListener listener = getListenerByArgument(argument);
        if (listener == null) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(new ConsoleEvent(listener.command()));
    }

    @SneakyThrows
    public void processCommand(@Nullable final String command) {
        if (command == null || command.isEmpty())
            throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    @Nullable
    private AbstractListener getListenerByArgument(@NotNull final String argument) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.argument())) return listener;
        }
        return null;
    }

}
