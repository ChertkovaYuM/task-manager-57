package ru.tsc.chertkova.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.ITokenService;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    public abstract String command();

    @Nullable
    public abstract String description();

    @Nullable
    public abstract String argument();

    public abstract void handler(ConsoleEvent event) throws Exception;;

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    @Override
    public String toString() {
        @Nullable final String name = command();
        @Nullable final String argument = argument();
        @Nullable final String description = description();
        @NotNull String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

}
