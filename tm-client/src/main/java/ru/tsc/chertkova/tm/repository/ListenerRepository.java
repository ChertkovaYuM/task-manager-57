package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.api.repository.IListenerRepository;
import ru.tsc.chertkova.tm.listener.AbstractListener;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public class ListenerRepository implements IListenerRepository {

    @NotNull
    private final Map<String, AbstractListener> mapByName = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractListener> mapByArgument = new TreeMap<>();

    @NotNull
    @Override
    public Collection<AbstractListener> getTerminalCommands() {
        return mapByName.values();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getCommands() {
        return mapByName.values();
    }

    public void add(@Nullable final AbstractListener command) {
        if (command == null) return;
        @Nullable final String name = command.command();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        @Nullable String argument = command.argument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @Nullable
    public AbstractListener getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractListener> getCommandsWithArgument() {
        return mapByArgument.values();
    }
}
