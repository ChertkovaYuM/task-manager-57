package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.listener.AbstractListener;

import java.util.Collection;

public interface IListenerService {

    void add(@NotNull AbstractListener command);

    @Nullable
    AbstractListener getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Iterable<AbstractListener> getCommandsWithArgument();

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

}
