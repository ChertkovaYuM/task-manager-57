package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.listener.AbstractListener;

import java.util.Collection;

public interface IListenerRepository {

    void add(@Nullable AbstractListener command);

    @Nullable
    AbstractListener getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @NotNull
    Iterable<AbstractListener> getCommandsWithArgument();

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

}
