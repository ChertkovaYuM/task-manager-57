package ru.tsc.chertkova.tm.listener;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.IListenerService;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.listener.AbstractListener;
import ru.tsc.chertkova.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    private IListenerService commandService;

    @Autowired
    private IPropertyService propertyService;

    @Nullable
    public String argument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
