package ru.tsc.chertkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectCreateRequest;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractProjectListener;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectCreateListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        //System.out.println("ENTER DATE BEGIN:");
        //@Nullable final Date dateBegin = TerminalUtil.nextDate();
        //System.out.println("ENTER DATE END:");
        //@Nullable final Date dateEnd = TerminalUtil.nextDate();
        projectEndpoint.createProject(new ProjectCreateRequest(getToken(), name, description));
    }

}
