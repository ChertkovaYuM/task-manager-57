package ru.tsc.chertkova.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.repository.IListenerRepository;
import ru.tsc.chertkova.tm.api.service.IListenerService;
import ru.tsc.chertkova.tm.listener.AbstractListener;

import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ListenerService implements IListenerService {

    @Nullable
    @Autowired
    private IListenerRepository commandRepository;

    @NotNull
    @Override
    public Collection<AbstractListener> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Nullable
    @Override
    public void add(@Nullable final AbstractListener command) {
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public Iterable<AbstractListener> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

}
