package ru.tsc.chertkova.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.data.DataBinarySaveRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractDomainListener;

@Component
public final class DomainBinarySaveListener extends AbstractDomainListener {

    @NotNull
    public static final String NAME = "data-save-bin";

    @NotNull
    public static final String DESCRIPTION = "Save date in binary file.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainBinarySaveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA SAVE BINARY]");
        getDomainEndpoint().saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
