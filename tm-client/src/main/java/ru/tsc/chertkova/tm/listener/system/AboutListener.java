package ru.tsc.chertkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractSystemListener;

@Component
public final class AboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        @NotNull final String authorName = getPropertyService().getAuthorName();
        @NotNull final String authorEmail = getPropertyService().getAuthorEmail();
        System.out.println("Name: " + authorName);
        System.out.println("E-mail: " + authorEmail);
    }

}
