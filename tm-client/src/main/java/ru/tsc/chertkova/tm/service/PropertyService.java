package ru.tsc.chertkova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key,
                                  @NotNull final String defaultValue) {
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperties().getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY);
    }

    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

}
