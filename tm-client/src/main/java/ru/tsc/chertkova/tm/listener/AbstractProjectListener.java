package ru.tsc.chertkova.tm.listener;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.chertkova.tm.listener.AbstractListener;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.util.DateUtil;

import java.util.List;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @Nullable
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    public String argument() {
        return null;
    }

    public void renderProjects(@Nullable final List<Project> projects) {
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    public void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        final Status status = project.getStatus();
        System.out.println("STATUS: " + status.getDisplayName());
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
