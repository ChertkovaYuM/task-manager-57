package ru.tsc.chertkova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class FileNameConst {

    @NotNull
    final public static String PROJECT_LOG_FILE_NAME = "./logger/project.log";

    @NotNull
    final public static String TASK_LOG_FILE_NAME = "./logger/task.log";

    @NotNull
    final public static String SESSION_LOG_FILE_NAME = "./logger/session.log";

    @NotNull
    final public static String USER_LOG_FILE_NAME = "./logger/user.log";

}
