package ru.tsc.chertkova.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.response.project.AbstractProjectResponse;
import ru.tsc.chertkova.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final Project project) {
        super(project);
    }

}
